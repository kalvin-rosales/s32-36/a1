const express = require('express');
const router = express.Router();

// Import units of functions from userController module
const {
	register,
	getAllUsers,
	checkEmail,
	login,
	profile,
	update,
	updatePw,
	adminStatus,
	userStatus,
	deleteUser,
	createCourse
	
} = require('./../controllers/userControllers');

const {verify, decode, verifyAdmin} = require('./../auth');

//GET ALL USERS
router.get('/', async (req, res) => {

	try{
		await getAllUsers().then(result => res.send(result))

	}catch(err){
		res.status(500).json(err)
	}
})


//REGISTER A USER 
router.post('/register', async (req, res) => {
	// console.log(req.body)	//user object

	try{
		await register(req.body).then(result => res.send(result))

	} catch(err){
		res.status(500).json(err)
	}
})


//CHECK IF EMAIL ALREADY EXISTS
router.post('/email-exists', async (req, res) => {
	try{
		await checkEmail(req.body).then(result => res.send(result))

	}catch(error){
		res.status(500).json(error)
	}
})



//LOGIN THE USER
	// authentication
router.post('/login', (req, res) => {
	// console.log(req.body)
	try{
		login(req.body).then(result => res.send(result))

	}catch(err){
		res.status(500).json(err)
	}
})



//RETRIEVE USER INFORMATION
router.get('/profile', verify, async (req, res) => {
	// console.log(req.headers.authorization)
	const userId = decode(req.headers.authorization).id
	// console.log(userId)

	try{
		profile(userId).then(result => res.send(result))

	}catch(err){
		res.status(500).json(err)
	}
})


//Create a route to update user information, make sure the route is secured with token
// router.put('/:id/update')
router.put('/update', verify, async (req, res) => {
	// console.log( decode(req.headers.authorization).id )

	const userId = decode(req.headers.authorization).id
	try{
		await update(userId, req.body).then(result => res.send(result))
	}catch(err){
		res.status(500).json(err)
	}
})

//Create a route /update-password to update user's password, make sure the  route is secured with token
router.patch('/update-password', verify, async (req, res) => {
	console.log( decode(req.headers.authorization).id )
	console.log(req.body.password)

	const userId = decode(req.headers.authorization).id
	try{
		await updatePw(userId, req.body.password).then(result => res.send(result))

	} catch(err){
		res.status(500).json(err)
	}
})



//Create a route /isAdmin to update user's isAdmin status to true, make sure the  route is secured with token
	//Only admin can update user's isAdmin status

// router.patch('/isAdmin', verify, async (req, res) => {
// 	// console.log(req.body)
// 	// console.log( decode(req.headers.authorization).isAdmin )
// 	const admin = decode(req.headers.authorization).isAdmin

// 	try{
// 		if(admin == true ){
// 			await adminStatus(req.body).then(result => res.send(result))

// 		} else {
// 			res.send(`You are not authorized!`)
// 		}

// 	}catch(err){
// 		res.status(500).json(err)
// 	}

// })

//OR

router.patch('/isAdmin', verifyAdmin, async (req, res) => {
	try{
		await adminStatus(req.body).then(result => res.send(result))

	}catch(err){
		res.status(500).json(err)
	}

})




//Create a route /isUser to update user's isAdmin status to false, make sure the  route is secured with token
	//Only admin can update user's isAdmin status
router.patch('/isUser', verifyAdmin, async (req, res) => {
	try{
		await userStatus(req.body).then(result => res.send(result))

	}catch(err){
		res.status(500).json(err)
	}
})




//Create a route /delete to delete a user from the DB and return true if successful, and make sure the route is secured
	// Only admin can delete a user
router.delete('/delete-user', verifyAdmin, async (req, res) => {
	try{
		deleteUser(req.body).then(result => res.send(result))

	}catch(err){
		res.status(500).json(err)
	}
})




//Create a route /create to create a new course, save the course in DB and return the document
	//Only admin can create a course




	router.post('/create', async (req, res) => {
	

		try{
			await createCourse(req.body).then(result => res.send(result))

		} catch(err){
			res.status(500).json(err)
		}
	})





//Export the router module to be used in index.js file
module.exports = router;
