const CryptoJS = require("crypto-js");
const User = require('./../models/User')
//const bcrypt = require('bcrypt');
const Course = require('./../models/Course')

//import createToken function from auth module
const {createToken} = require('./../auth');


//REGISTER A USER
module.exports.register = async (reqBody) => {
	// console.log(reqBody)
	const {firstName, lastName, email, password} = reqBody

	const newUser = new User({
		firstName: firstName,
		lastName: lastName, 
		email: email,
		password: CryptoJS.AES.encrypt(password, process.env.SECRET_PASS).toString()
		//password: bcrypt.hashSync(password, 10)
	})

	return await  newUser.save().then(result => {
		if(result){
			return true
		} else {
			if(result == null){
				return false
			}
		}
	})
}


//GET ALL USERS
module.exports.getAllUsers = async () => {

	return await User.find().then(result => result)
}



//CHECK OF EMAIL EXISTS
module.exports.checkEmail = async (reqBody) => {
	const{email} = reqBody

	return await User.findOne({email:email}).then((result,err) =>{
		if(result){
			return true
		} else {
			if(result == null){
				return false
			} else {
				return err
			}
		}
	})
}

// LOGIN A USER
module.exports.login = async (reqBody) =>{

	return await User.findOne({email: reqBody.email}).then((result, err)=> {
		if(result == null){
			return {message: `User does not exist.`}

		} else {

			if(result !== null){
				//check if password is correct
				const decryptedPw = CryptoJS.AES.decrypt(result.password, process.env.SECRET_PASS).toString(CryptoJS.enc.Utf8)

				console.log(reqBody.password==decryptedPw)

				if(reqBody.password == decryptedPw){
						//create a token for the user

						return {token:createToken(result)}			
				} else {
						return {auth: `Auth Failed`}
				}

			} else {
				return err
			}
		}
	})
}



//RETRIEVE USER INFORMATION
module.exports.profile = async (id) => {

	return await User.findById(id).then((result, err) => {
		if(result){
			return result
		} else {
			if(result == null){
				return {message:`user does not exist`}
			} else {
				return err
			}
		}
	})
}


//UPDATE NAME
// module.exports.update = async (name, reqBody) => {


// 	return await User.findOneAndUpdate({name: name}, {$set: reqBody}, {new:true})
// 	.then(result => {
// 		if(result == null){
// 			return {message: `no existing document`}
// 		} else {
// 			if(result != null){
// 				return result
// 			}
// 		}
// 	})
// }

// //UPDATE PASSWORD
// module.exports.updatepassword = async (pass, reqBody) => {


// 	return await User.findOneAndUpdate({pass: pass}, {$set: reqBody}, {new:true})
// 	.then(result => {
// 		if(result == null){
// 			return {message: `no existing document`}
// 		} else {
// 			if(result != null){
// 				return result
// 			}
// 		}
// 	})
// }

//DELETE USER

// module.exports.deleteUser = async (id) => {

// 	return await User.findByIdAndDelete(id).then( result => {
// 		try{
// 			if(result != null){
// 				
// 				return {message:`user has been deleted`}
// 			} else {
// 				return false
// 			}

// 		}catch(err){
// 			return err
// 		}
// 	})
// }


//ANSWER
//UPDATE USER INFO
module.exports.update = async (userId,reqBody) => {
	const userData ={
		firstName:reqBody.firstName,
		lastName: reqBody.lastName,
		email: reqBody.email,
		password: CryptoJS.AES.encrypt(reqBody.password, process.env.SECRET_PASS).toString()
	}

	return await User.findByIdAndUpdate(userId,{$set:userData},{new:true}).then((result,err) =>{	//console.log(result))
		if(result){
			result.password = "***"
			return result
		}else{
			return err
		}
	})
}

//UPDATE PASSWORD
module.exports.updatePw = async (id, password) => {
	let updatedPw = {
		password: CryptoJS.AES.encrypt(password, process.env.SECRET_PASS).toString()
	}

	return await User.findByIdAndUpdate(id, {$set: updatedPw})
	.then((result, err) => {
		if(result){
			result.password = "***"
			return result
		} else{
			return err
		}
	})
}



// CHANGE TO ADMIN STATUS TO TRUE
module.exports.adminStatus = async (reqBody) => {
	const {email} = reqBody

	return await User.findOneAndUpdate({email: email}, {$set: {isAdmin: true}}, {new:true}).then((result, err) => result ? true : err)
}

// CHANGE TO ADMIN STATUS TO FALSE
module.exports.userStatus = async (reqBody) => {

	return await User.findOneAndUpdate({email: reqBody.email}, {$set: {isAdmin: false}},{new:true}).then((result, err) => result ? true : err)
}


// DELETE A USER

module.exports.userDelete = async (reqBody) => {

	return await User.findOneAndDelete({email: reqBody.email}).then((result, err) => result ? true : err)
}




//Create a route /create to create a new course, save the course in DB and return the document
	//Only admin can create a course

	module.exports.createCourse = async (reqBody) => {
		// console.log(reqBody)
		const {courseName, description, price} = reqBody

		const newCourse = new Course({
			courseName: courseName,
			description: description, 
			price: price
			
		})

		return await  newUser.save().then(result => {
			if(result){
				return true
			} else {
				if(result == null){
					return false
				}
			}
		})
	}