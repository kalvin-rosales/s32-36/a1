const express = require('express');
const mongoose = require('mongoose');
const dotenv = require('dotenv').config();
const cors = require('cors')

const PORT = process.env.PORT || 3007;
const app = express();



// Connect our routes module 
const userRoutes = require('./routes/userRoutes');
const courseRoutes = require('./routes/courseRoutes')


//Middleware to handle JSON payloads
app.use(express.json())
app.use(express.urlencoded({extended:true}))
app.use(cors())
// prevents blocking of request from client specially different domains

//Connect Database to server
mongoose.connect(process.env.MONGO_URL, {useNewUrlParser: true, useUnifiedTopology: true})

// Test Db Connection
const db = mongoose.connection;
db.on("error", console.error.bind(console, 'connection error:'))
db.once("open", () => console.log(`Connected to Database`))



// SCHEMA
	//schema is in models module



//ROUTES
	//create a middleware to be the root url of all routes
app.use(`/api/users`, userRoutes);
app.use(`/api/courses`, courseRoutes);






app.listen(PORT, () => console.log(`Server connected to the ${PORT}`))